import getLineArray from './src/getLineArray.js'
import getLineGroups from './src/getLineGroups'
import getLineStats from './src/getLineStats'

export default function tokenizeString(plainText: string) {
  const lines = getLineArray(plainText)
  const stats = lines.map(getLineStats)
  const tokens = getLineGroups(stats)

  return tokens
}
