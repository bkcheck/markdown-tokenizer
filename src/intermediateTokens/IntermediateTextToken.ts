export enum IntermediateTextTokenType {
  Text = 0,
  Emphasis = 1,
  Strikethrough = 2,
  Code = 3,
}

export default class IntermediateTextToken {
  public char: string | null = null
  public length: number | null = null
  public text: string | null = null
  public type: IntermediateTextTokenType

  constructor(init: Partial<IntermediateTextToken>) {
    Object.assign(this, init)
  }
}
