import UnorderedListToken from '../tokens/UnorderedListToken'
import UnorderedListItemToken from '../tokens/UnorderedListItemToken'

export default class IntermediateUnorderedListToken extends UnorderedListToken {
  public indent: number = 0
  public parent: IntermediateUnorderedListToken = null

  constructor(indent: number, parent?: IntermediateUnorderedListToken) {
    super()

    this.indent = indent
    this.parent = parent
  }

  add(token: UnorderedListItemToken | IntermediateUnorderedListToken) : IntermediateUnorderedListToken {
    this.children.push(token)
    return this
  }
}
