import ContainerToken from './ContainerToken'
import OrderedListItemToken from './OrderedListItemToken'

export default class OrderedListToken extends ContainerToken {
  constructor(children?: OrderedListItemToken[]) {
    super('orderedList')

    this.children = children || []
  }

  add (token: OrderedListItemToken): OrderedListToken {
    this.children.push(token)
    return this
  }
}
