import TextToken from './TextToken'

export default class InlineCodeToken extends TextToken {
  constructor (innerText?: string, children?: TextToken[]) {
    super(innerText, children || [], 'inlineCode')
  }
}
