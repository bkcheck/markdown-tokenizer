import TextToken from './TextToken'

export default class EmphasisTextToken extends TextToken {
  public emphasisLevel: number

  constructor (emphasisLevel?: number, innerText?: string, children?: TextToken[]) {
    super(innerText, children, 'emphasisText')

    this.emphasisLevel = emphasisLevel || 1
  }
}
