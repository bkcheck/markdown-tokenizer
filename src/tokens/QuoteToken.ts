import TextToken from './TextToken'

export default class QuoteToken extends TextToken {
  constructor (innerText?: string) {
    super(innerText, null, 'quote')
  }
}
