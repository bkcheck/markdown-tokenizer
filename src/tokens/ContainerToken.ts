import BaseToken from './BaseToken'

export default class ContainerToken extends BaseToken {
  public children: BaseToken[]

  constructor (type: string, children?: BaseToken[]) {
    super(type)

    this.children = children || []
  }
}
