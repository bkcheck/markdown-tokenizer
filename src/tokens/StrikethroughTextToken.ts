import TextToken from './TextToken'

export default class StrikethroughTextToken extends TextToken {
  constructor(innerText?: string, children?: TextToken[]) {
    super(innerText, children || [], 'strikethroughText')
  }
}
