import Token from './BaseToken'

export default class TextToken extends Token {
  public children: Token[]
  public innerText: string

  constructor (innerText?: string, children?: TextToken[], type?: string) {
    super(type || 'text')
    this.children = children || []
    this.innerText = innerText || ''
  }

  add (token: TextToken): TextToken {
    if (!token) {
      return
    }

    this.children.push(token)

    return this
  }
}
