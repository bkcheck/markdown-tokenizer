import ContainerToken from './ContainerToken'
import UnorderedListItemToken from './UnorderedListItemToken'

export default class UnorderedListToken extends ContainerToken {
  constructor(children?: Array<UnorderedListItemToken | UnorderedListToken>) {
    super('unorderedList', children || [])
  }

  add(token: UnorderedListItemToken | UnorderedListToken): UnorderedListToken {
    this.children.push(token)
    return this
  }
}
