import ContainerToken from './ContainerToken'
import TextToken from './TextToken'

export default class CodeBlockToken extends ContainerToken {
  public language: string

  constructor (language?: string, children?: TextToken[]) {
    super('codeBlock', children || [])

    this.language = language || null
  }

  add (token: TextToken) {
    if (!token || token.type !== 'text') {
      if (token.type) {
        console.warn(`Can't add a non-text token to a code block.`)
      }
      return
    }

    this.children.push(token)
  }
}
