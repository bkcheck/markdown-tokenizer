import TextToken from './TextToken'

export default class OrderedListItemToken extends TextToken {
  constructor(innerText?: string, children?: TextToken[]) {
    super(innerText, children, 'orderedListItem')
  }
}
