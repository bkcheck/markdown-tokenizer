import BaseToken from './BaseToken'

export default class LinkToken extends BaseToken {
  public innerText: string
  public url: string

  constructor (url: string, innerText: string, type?: string) {
    super(type || 'link')

    this.innerText = innerText
    this.url = url
  }
}
