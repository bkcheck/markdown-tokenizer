import BaseToken from './BaseToken'

export default class HeaderToken extends BaseToken {
  public headerLevel: number
  public innerText: string

  constructor (headerLevel: number, innerText?: string) {
    super('header')

    this.headerLevel = headerLevel
    this.innerText = innerText
  }
}
