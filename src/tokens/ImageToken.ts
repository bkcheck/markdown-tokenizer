import  LinkToken from './LinkToken'

export default class ImageToken extends LinkToken {
  public title: string

  constructor (url: string, innerText?: string, title?: string) {
    super(url, innerText, 'image')

    this.title = title || innerText
  }
}
