import TextToken from './TextToken'

export default class UnorderedListItemToken extends TextToken {
  constructor(innerText?: string, children?: TextToken[]) {
    super(innerText, children, 'unorderedListItem')
  }
}
