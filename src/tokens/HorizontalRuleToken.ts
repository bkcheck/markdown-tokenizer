import BaseToken from './BaseToken'

export default class HorizontalRuleToken extends BaseToken {
  constructor () {
    super('horizontalRule')
  }
}
