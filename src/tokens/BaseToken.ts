export default class BaseToken {
  public type: string

  constructor (type: string) {
    if (!type) {
      throw new Error('A type is required to instantiate a Token element')
    }
    this.type = type
  }
}
