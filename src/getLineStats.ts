import CharCode from './CharCode'
import LineStats from './LineStats'
import LineType from './LineType'

import getLineType from './getLineType'

export default function getLineStats(rawString: string): LineStats {
  if (!rawString) {
    return new LineStats()
  }

  let leadingWhitespace = 0
  let leadingChars = ''

  let i = 0
  let whitespaceComplete = false
  while (i < rawString.length && !whitespaceComplete) {
    const unicode = rawString.codePointAt(i)

    if (unicode === CharCode.SPACE || unicode === CharCode.TAB) {
      leadingWhitespace += (unicode === CharCode.TAB ? 2 : 1)
      i++
    } else {
      whitespaceComplete = true
    }
  }

  let leadingCharsComplete = false
  while (i < rawString.length && !leadingCharsComplete) {
    const unicode = rawString.codePointAt(i)

    if (unicode !== CharCode.SPACE && unicode !== CharCode.TAB) {
      leadingChars += rawString[i]
    } else {
      leadingCharsComplete = true
    }

    i++
  }

  const remainder = rawString.slice(i)

  // If the line is just whitespace, just return an empty Stats object
  if (!leadingChars && !remainder) {
    return new LineStats(leadingWhitespace)
  }

  const lineType = getLineType(leadingChars, remainder)

  if (lineType !== LineType.Normal) {
    return new LineStats(leadingWhitespace, lineType, remainder, leadingChars)
  } else {
    return new LineStats(leadingWhitespace, lineType, [leadingChars, remainder].join(' '))
  }
}
