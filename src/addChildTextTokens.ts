import getTextTokenStack from './getTextTokenStack'

import EmphasisTextToken from './tokens/EmphasisTextToken'
import InlineCodeToken from './tokens/InlineCodeToken'
import StrikethroughTextToken from './tokens/StrikethroughTextToken'
import TextToken from './tokens/TextToken'
import IntermediateTextToken, { IntermediateTextTokenType } from './intermediateTokens/IntermediateTextToken'

/**
 * Checks to see if the parent has any innerText before appending. If it does,
 * promote this to a separate TextToken before pushing a child.
 * @param {Object} parent
 * @param {Object} token
 */
function addChildTokenToParent(parent: TextToken, token: TextToken) {
  if (parent.innerText) {
    parent.add(new TextToken(parent.innerText))
    parent.innerText = ''
  }
  parent.add(token)
}

/**
 * Adds text to a parent element either by appending to the innerText or pushing a new
 * TextToken as a child.
 * @param {Object} parent
 * @param {string} text
 */
function appendTextToParent(parent: TextToken, text: string): void {
  if (parent.children.length) {
    // If the parent's most recent child is also a text token, just push to this token
    // instead of appending a new one.
    const lastChild = parent.children[parent.children.length - 1]
    if (lastChild.type === 'text') {
      (lastChild as TextToken).innerText += text
    } else {
      addChildTokenToParent(parent, new TextToken(text))
    }
  } else {
    // console.log(`Appending ${text} to token ${parent.type} with innerText ${parent.innerText}`)
    parent.innerText = text
  }
}

function createTokenForType(type: IntermediateTextTokenType): EmphasisTextToken | InlineCodeToken | StrikethroughTextToken {
  switch (type) {
    case IntermediateTextTokenType.Emphasis:
      return new EmphasisTextToken()
    case IntermediateTextTokenType.Code:
      return new InlineCodeToken()
    case IntermediateTextTokenType.Strikethrough:
      return new StrikethroughTextToken()
  }
}

function getStackIndexOfClosingToken(stack: IntermediateTextToken[], startIndex: number): number {
  const { length, type } = stack[startIndex]

  for (let i = startIndex + 1; i < stack.length; i++) {
    if (stack[i].type === type && stack[i].length === length) {
      return i
    }
  }

  return null
}

function pushChildTokensFromTokenStack(stack: IntermediateTextToken[], parent: TextToken): void {
  let i = 0
  while (i < stack.length) {
    const { char, text, type, length } = stack[i]
    // console.log('Processing', char, text, type, length)

    // If this is a text token just append the text to the currently open parent token
    // unless the parent has children in which case we want to append it as a new text token
    if (type === IntermediateTextTokenType.Text) {
      // console.log('Appending', text)
      appendTextToParent(parent, text)
      i++
      continue
    }

    // If this is any other token type, find the index of the matching closing token
    const closingTokenIndex = getStackIndexOfClosingToken(stack, i)

    if (closingTokenIndex !== null) {
      const subArray = stack.slice(i + 1, closingTokenIndex)
      const nextToken = createTokenForType(type)
      
      pushChildTokensFromTokenStack(subArray, nextToken)

      if (type === IntermediateTextTokenType.Emphasis) {
        (nextToken as EmphasisTextToken).emphasisLevel = Math.min(length, 3)
      }

      addChildTokenToParent(parent, nextToken)

      i = closingTokenIndex + 1
    } else {
      // We didn't find any matching closing token so just treat this as text
      appendTextToParent(parent, Array(length).fill(char).join(''))
      i++
    }
  }
}

/**
 * Parses a plaintext string into TextTokens. The parent token is required here
 * as well because the text may be appended to the parent's innerText prop instead
 * of appended as a child.
 */
function addChildTextTokens(text: string, parent: TextToken): TextToken {
  const stack = getTextTokenStack(text)

  pushChildTokensFromTokenStack(stack, parent)

  return parent
}

export default addChildTextTokens
