enum LineType {
  Empty = 0,
  Normal = 1,
  Header = 2,
  OrderedList = 3,
  UnorderedList = 4,
  CodeBlock = 5,
  Quote = 6,
  HorizontalRule = 7,
  Image = 8
}

export default LineType
