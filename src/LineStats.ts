import LineType from './LineType'

export default class LineStats {
  public leadingChars: string
  public leadingWhitespace: number
  public remainder: string
  public type: LineType

  constructor(leadingWhitespace?: number, type?: LineType, remainder?: string, leadingChars?: string) {
    this.leadingWhitespace = leadingWhitespace || 0
    this.leadingChars = leadingChars
    this.remainder = remainder
    this.type = type || LineType.Empty
  }
}
