import BaseToken from './tokens/BaseToken'
import LineGroup from './LineGroup'
import LineStats from './LineStats'
import LineType from './LineType'
import getTokenForLineGroup from './getTokenForLineGroup'

/**
 * Certain types of adjacent lines can be collapsed down into one
 * token. For instance, adjacent lines of text should be treated as
 * one larger block of text for processing purposes. We can group up
 * code blocks and lists into one data structure so we can analyze the
 * whole thing at once.
 */
export default function getLineGroups(lineArray: LineStats[]): BaseToken[] {
  const tokens: BaseToken[] = []

  let lineGroup: LineGroup = null
  for (let i = 0; i < lineArray.length; i++) {
    const line = lineArray[i]

    switch (line.type) {
      case LineType.Empty:
        // If this is an empty line, push any existing lineGroup but
        // ignore the current line.
        if (lineGroup) {
          // Code blocks should only be terminated by another code block line, so
          // don't push this group because of an empty line.
          if (lineGroup.type === LineType.CodeBlock) {
            lineGroup.add(line)
          } else {
            // All other group types should get pushed.
            tokens.push(getTokenForLineGroup(lineGroup))
            lineGroup = null
          }
        }
        break
      case LineType.Header:
      case LineType.HorizontalRule:
      case LineType.Image:
        // These line types do not group, so push any existing lineGroup
        // and push up the processed line.
        if (lineGroup) {
          tokens.push(getTokenForLineGroup(lineGroup))
          lineGroup = null
        }
        tokens.push(getTokenForLineGroup(new LineGroup(line.type, line)))
        break
      case LineType.Normal:
      case LineType.Quote:
      case LineType.UnorderedList:
      case LineType.OrderedList:
        // These line types group, so check to see if the current line should
        // be added to the group or pushed up.
        if (lineGroup) {
          const groupType = lineGroup.type
          if (groupType === line.type ||
              (groupType === LineType.CodeBlock && line.type === LineType.Normal)) {
            // Groups are the same type so add to the group.
            // Allow normal lines to be added to code block groups.
            lineGroup.add(line)
          } else {
            // Groups are different types so terminate the previous one and
            // start a new one containing this line
            tokens.push(getTokenForLineGroup(lineGroup))
            lineGroup = new LineGroup(line.type, line)
          }
        } else {
          // Start a new lineGroup containing the current line
          lineGroup = new LineGroup(line.type, line)
        }
        break
      case LineType.CodeBlock:
        // Code blocks work a little differently in that they are bookended
        // by code block type lines.
        if (lineGroup) {
          if (lineGroup.type === LineType.CodeBlock) {
            // If there is an open code block, this line ends it
            tokens.push(getTokenForLineGroup(lineGroup))
            lineGroup = null
          } else {
            // Terminate and push the previous line group
            tokens.push(getTokenForLineGroup(lineGroup))
            lineGroup = new LineGroup(LineType.CodeBlock, line)
          }
        } else {
          // Start a new lineGroup. We need to push the opening line
          // becuase it could contain info about the code block language.
          lineGroup = new LineGroup(LineType.CodeBlock, line)
        }
        break
    }
  }

  // Push our last lineGroup since the end of the document terminates
  // this one.
  if (lineGroup) {
    tokens.push(getTokenForLineGroup(lineGroup))
  }

  return tokens
}
