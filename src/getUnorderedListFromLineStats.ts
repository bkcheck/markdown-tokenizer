import IntermediateUnorderedListToken from './intermediateTokens/IntermediateUnorderedListToken'
import LineStats from '../src/LineStats'
import UnorderedListToken from '../src/tokens/UnorderedListToken'
import UnorderedListItemToken from '../src/tokens/UnorderedListItemToken'

import addChildTextTokens from './addChildTextTokens'

export default function getUnorderedListFromLineStats(lines: LineStats[]): UnorderedListToken {
  let currentParent: IntermediateUnorderedListToken = null

  function moveUpOneLevel() {
    const temp = currentParent
    currentParent = temp.parent
    delete temp.parent
    delete temp.indent
  }

  for (let i = 0; i < lines.length; i++) {
    const line = lines[i]
    const nextIndent = Math.floor(line.leadingWhitespace / 2)

    // First time through the loop. Create a new UnorderedList and add this line as a list item
    if (!currentParent) {
      currentParent = new IntermediateUnorderedListToken(nextIndent)
      currentParent.add(addChildTextTokens(line.remainder, new UnorderedListItemToken()))
      continue
    }

    // We're on the same level as the current parent so just add a new list item.
    if (nextIndent === currentParent.indent) {
      currentParent.add(addChildTextTokens(line.remainder, new UnorderedListItemToken()))
      continue
    }

    // Need to create a new child of the current parent and set that as the current parent
    if (nextIndent > currentParent.indent) {
      const nextItem = new IntermediateUnorderedListToken(nextIndent, currentParent)
      nextItem.add(addChildTextTokens(line.remainder, new UnorderedListItemToken()))

      currentParent.add(nextItem)
      currentParent = nextItem
      continue
    }

    // If we are here, we are "un-indenting". We need to move back up the chain,
    // but we can't assume our next line should be added to the parent of the current
    // parent...we could un-indent by more than one level. e.g:
    // - Level 1
    //   - Level 1.1      <- This is the current parent object
    //     - Level 1.1.1  <- This is the previous line
    // - Level 2          <- This is the current line but it has jumped two levels up
    const levelsUp = currentParent.indent - nextIndent

    for (let j = 0; j < levelsUp; j++) {
      moveUpOneLevel()
    }

    currentParent.add(addChildTextTokens(line.remainder, new UnorderedListItemToken()))
  }

  // We need to move back up the chain to find the top-level parent
  while (currentParent.parent) {
    moveUpOneLevel()
  }

  return new UnorderedListToken(currentParent.children as UnorderedListItemToken[])
}
