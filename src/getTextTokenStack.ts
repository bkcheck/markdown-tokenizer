import CharCode from './CharCode'
import IntermediateTextToken, { IntermediateTextTokenType } from './intermediateTokens/IntermediateTextToken'

const { ASTERISK, LEFT_PARENTHESIS, RIGHT_PARENTHESIS, LEFT_BRACKET, RIGHT_BRACKET, BACKSLASH,
  UNDERSCORE, BACKTICK, TILDE } = CharCode

const delimitedChars = [ASTERISK, UNDERSCORE, BACKTICK, TILDE]
const allSpecialChars = delimitedChars.concat([BACKSLASH, LEFT_BRACKET, RIGHT_BRACKET, LEFT_PARENTHESIS, RIGHT_PARENTHESIS])

function getIndexOfNextDifferentCharacter(text: string, startIndex: number): number {
  const char = text[startIndex]

  for (let i = startIndex + 1; i < text.length; i++) {
    if (text[i] !== char) {
      return i
    }
  }

  return text.length
}

function getTokenTypeForDelimitedChar(code: number): IntermediateTextTokenType {
  switch (code) {
    case ASTERISK:
    case UNDERSCORE:
      return IntermediateTextTokenType.Emphasis
    case TILDE:
      return IntermediateTextTokenType.Strikethrough
    case BACKTICK:
      return IntermediateTextTokenType.Code
  }
}

export default function getTextTokenStack(text: string): IntermediateTextToken[] {
  const stack: IntermediateTextToken[] = []

  // Loop through text and pull out token delimiters. For now,
  // don't make decisions about how these should be nested or
  // rendered into actual tokens, just record details about each
  // delimiter.
  let i = 0
  let currentText = ''
  while (i < text.length) {
    const char = text[i]
    const code = text.charCodeAt(i)

    // A backslash indicates a delimited character
    if (code === BACKSLASH && allSpecialChars.includes(text.charCodeAt(i + 1))) {
      currentText += text[i + 1]
      i += 2
      continue
    }

    // We can handle any delimited characters the same for now.
    // Just translate it into a token type.
    if (delimitedChars.includes(code)) {

      const nextIndex = getIndexOfNextDifferentCharacter(text, i)
      const length = nextIndex - i
      const type = getTokenTypeForDelimitedChar(code)

      if (type === IntermediateTextTokenType.Strikethrough && length < 2) {
        // Markup requires strikethrough be delimited with two tildes so don't even
        // count this as a real strikethrough
        currentText += text.substr(i, length)
      } else {
        // Push any existing text as a text token first
        if (currentText) {
          stack.push(new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: currentText }))
          currentText = ''
        }
        stack.push(new IntermediateTextToken({ type, length, char }))
      }

      i += length
      continue
    }

    currentText += char
    i += 1
  }

  if (currentText) {
    stack.push(new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: currentText }))
  }

  return stack
}
