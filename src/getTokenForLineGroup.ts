import BaseToken from './tokens/BaseToken'
import CodeBlockToken from './tokens/CodeBlockToken'
import HeaderToken from './tokens/HeaderToken'
import HorizontalRuleToken from './tokens/HorizontalRuleToken'
import ImageToken from './tokens/ImageToken'
import LineStats from './LineStats'
import LineType from './LineType'
import OrderedListItemToken from './tokens/OrderedListItemToken'
import OrderedListToken from './tokens/OrderedListToken'
import QuoteToken from './tokens/QuoteToken'
import TextToken from './tokens/TextToken'

import addChildTextTokens from './addChildTextTokens'
import getUnorderedList from './getUnorderedListFromLineStats'
import LineGroup from './LineGroup'

const imageRegex = /^!\[(.*)]\(([^ ]*) *(.*)\)$/
const languageRegex = /^```([a-zA-Z0-9]+)$/

function coalesceText(lines: LineStats[]): string {
  return lines.map(x => x.remainder.trim()).join(' ')
}

function getCoalescedTextToken(lines: LineStats[]): TextToken | QuoteToken {
  const text = coalesceText(lines)
  const type = lines[0].type

  const parent = type === LineType.Quote ? new QuoteToken() : new TextToken()
  addChildTextTokens(text, parent)

  return parent
}

function getCodeBlock (lines: LineStats[]): CodeBlockToken {
  // We can pop the first element from the array here becuase it
  // just contains info about the language, if anything.
  const { leadingChars } = lines.shift()
  const match = languageRegex.exec(leadingChars)
  const language = match && match[1]

  const codeBlock = new CodeBlockToken(language)

  // May not want to treat each line in a code block as a separate text token but
  // we definitely need to preserve leading whitespace.
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i]
    const innerText = `${Array(line.leadingWhitespace).fill(' ').join('')}${line.remainder}`
    codeBlock.add(new TextToken(innerText))
  }

  return codeBlock
}

function getHeaderToken(line: LineStats): HeaderToken {
  return new HeaderToken(line.leadingChars.length, line.remainder)
}

function getImageToken(line: LineStats): ImageToken {
  const [_, text, url, title] = imageRegex.exec(line.remainder)
  return new ImageToken(url, text, title)
}

function getOrderedList(lines: LineStats[]): OrderedListToken {
  const list = new OrderedListToken()

  for (let i = 0; i < lines.length; i++) {
    let item = addChildTextTokens(lines[i].remainder, new OrderedListItemToken())
    list.add(item)
  }

  return list
}

export default function getTokenForLineGroup(lineGroup: LineGroup): BaseToken {
  switch (lineGroup.type) {
    case LineType.Header:
      return getHeaderToken(lineGroup.lines[0])
    case LineType.HorizontalRule:
      return new HorizontalRuleToken()
    case LineType.Image:
      return getImageToken(lineGroup.lines[0])
    case LineType.Normal:
    case LineType.Quote:
      return getCoalescedTextToken(lineGroup.lines)
    case LineType.OrderedList:
      return getOrderedList(lineGroup.lines)
    case LineType.UnorderedList:
      return getUnorderedList(lineGroup.lines)
    case LineType.CodeBlock:
      return getCodeBlock(lineGroup.lines)
  }
}
