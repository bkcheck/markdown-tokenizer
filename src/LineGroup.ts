import LineStats from './LineStats'
import LineType from './LineType'

export default class LineGroup {
  public lines: LineStats[]
  public type: LineType

  constructor(type: LineType, initialLine?: LineStats) {
    this.type = type
    this.lines = initialLine ? [initialLine] : []
  }

  add(line: LineStats) {
    this.lines.push(line)
  }
}