export default function getLineArray (rawString: string): string[] {
  const lineArray: string[] = []

  let current: string = null

  for (let i = 0; i < rawString.length; i++) {
    const unicode = rawString.codePointAt(i)

    if (unicode === 0x0A || unicode === 0x0D) {
      lineArray.push(current)
      current = null
    } else {
      current = (current || '') + rawString[i]
    }
  }

  if (current !== null) {
    lineArray.push(current)
  }

  return lineArray
}
