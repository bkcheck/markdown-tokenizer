import CharCode from './CharCode'
import LineType from './LineType'

// This could be improved by replacing the .*s with some actual character classes
const imageRegex = /^!\[.*]\(.*\)$/

function isCodeBlock(leadingChars: string): boolean {
  for (let i = 0; i <= 2; i++) {
    if (leadingChars.codePointAt(i) !== CharCode.BACKTICK) {
      return false
    }
  }
  return true
}

function isHeader(leadingChars: string): boolean {
  if (leadingChars.length > 6) {
    return false
  }

  // All characters should be a # sign
  for (let i = 0; i < leadingChars.length; i++) {
    if (leadingChars.codePointAt(i) !== CharCode.HASH) {
      return false
    }
  }

  return true
}

function isHorizontalRule(leadingChars: string): boolean {
  if (leadingChars.length < 3) {
    return false
  }

  // All chars should be a dash
  for (let i = 0; i < leadingChars.length; i++) {
    if (leadingChars.codePointAt(i) !== CharCode.DASH) {
      return false
    }
  }

  return true
}

function isImage(leadingChars: string, remainder: string): boolean {
  // An image tag will have no leading characters.
  if (leadingChars) {
    return false
  }

  // An image tag takes the form ![text](url title)
  // For now we will just use a regex to parse this. May need some better
  // logic around this in the future, hopefully not.
  return !!(imageRegex.exec(remainder))
}

function isQuote(leadingChars: string): boolean {
  return leadingChars.length === 1 &&
    leadingChars.codePointAt(0) === CharCode.GREATER_THAN
}

function isOrderedList(leadingChars: string): boolean {
  const start = leadingChars.codePointAt(0)
  const end = leadingChars.codePointAt(leadingChars.length - 1)

  return start >= CharCode.DIGIT_ZERO
    && start <= CharCode.DIGIT_NINE
    && end === CharCode.PERIOD
}

function isUnorderedList(leadingChars: string): boolean {
  if (leadingChars.length !== 1) {
    return false
  }

  const pt = leadingChars.codePointAt(0)

  return [CharCode.DASH, CharCode.ASTERISK, CharCode.PLUS].includes(pt)
}

/**
 * Accepts the first non-whitespace characters up to the next whitespace
 * and returns the type of line based on these characters. For instance,
 * given the string:
 * '  ### Hello'
 * This function should be passed the string '###' and will determine that this
 * is a header line. Likewise, the string:
 * '1. Hello again'
 * Should get '1.' and will return that this is an ordered list. If the line
 * is a normal test line, e.g.
 * 'Nothing going on here'
 * This function will see the string 'Nothing' and determine that it is a normal
 * line of text.
 * Remainder is passed in solely to determine image tags
 */
export default function getLineType(leadingChars: string, remainder: string): LineType {
  if (!leadingChars) {
    if (isImage(leadingChars, remainder)) {
      return LineType.Image
    }
    return LineType.Empty
  }
  
  if (isHeader(leadingChars)) {
    return LineType.Header
  }

  if (isOrderedList(leadingChars)) {
    return LineType.OrderedList
  }

  if (isUnorderedList(leadingChars)) {
    return LineType.UnorderedList
  }

  if (isCodeBlock(leadingChars)) {
    return LineType.CodeBlock
  }

  if (isQuote(leadingChars)) {
    return LineType.Quote
  }

  if (isHorizontalRule(leadingChars)) {
    return LineType.HorizontalRule
  }

  return LineType.Normal
}
