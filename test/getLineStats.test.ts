import getLineStats from '../src/getLineStats';
import LineStats from '../src/LineStats'
import LineType from '../src/LineType'
import { expect } from 'chai';
import 'mocha';

describe('getLineStats tests', () => {

  it('Returns empty LineStats for an empty string', () => {
    const input = ''
    const result = getLineStats(input)

    expect(result).to.eql(new LineStats())
  })

  it('Returns empty LineStats for whitespace-only string', () => {
    const input = '   \t'
    const result = getLineStats(input)

    expect(result).to.eql(new LineStats(5))
  })

  it('Calculates whitespace correctly', () => {
    const input = '   \t-'
    const result = getLineStats(input)

    expect(result).to.eql(new LineStats(5, LineType.UnorderedList, '', '-'))
  })

  it('Correctly parses remainder text for ordered lists', () => {
    const input = ' 1. hello'
    const result = getLineStats(input)

    expect(result).to.eql(new LineStats(1, LineType.OrderedList, 'hello', '1.'))
  })

  it('Correctly parses remainder text for code blocks', () => {
    const input = '```language'
    const result = getLineStats(input)

    expect(result).to.eql(new LineStats(0, LineType.CodeBlock, '', '```language'))
  })

  it('Correctly parses Normal type lines', () => {
    const input = '   normal line'
    const result = getLineStats(input)

    expect(result).to.eql(new LineStats(3, LineType.Normal, 'normal line'))
  })

})