import getUnorderedListFromLineStats from '../src/getUnorderedListFromLineStats'
import { expect } from 'chai'
import 'mocha'

import LineStats from '../src/LineStats'
import LineType from '../src/LineType'
import UnorderedListToken from '../src/tokens/UnorderedListToken'
import UnorderedListItemToken from '../src/tokens/UnorderedListItemToken'

describe('getUnorderedListFromLineStats tests', () => {

  it('Correctly parses single-level unordered lists', () => {
    const input = [
      new LineStats(0, LineType.UnorderedList, 'line1', '-'),
      new LineStats(0, LineType.UnorderedList, 'line2', '-'),
    ]

    const expected = new UnorderedListToken([
      new UnorderedListItemToken('line1'),
      new UnorderedListItemToken('line2'),
    ])
    const actual = getUnorderedListFromLineStats(input)

    expect(actual).to.eql(expected)
  })

  it('Correctly parses sub-levels', () => {
    const input = [
      new LineStats(0, LineType.UnorderedList, 'line1', '-'),
      new LineStats(2, LineType.UnorderedList, 'line2', '-'),
    ]

    const expected = new UnorderedListToken([
      new UnorderedListItemToken('line1'),
      new UnorderedListToken([
        new UnorderedListItemToken('line2')
      ]),
    ])
    const actual = getUnorderedListFromLineStats(input)

    expect(actual).to.eql(expected)
  })

  it('Correctly un-indents', () => {
    const input = [
      new LineStats(0, LineType.UnorderedList, 'line1', '-'),
      new LineStats(2, LineType.UnorderedList, 'line2', '-'),
      new LineStats(4, LineType.UnorderedList, 'line3', '-'),
      new LineStats(0, LineType.UnorderedList, 'line4', '-'),
    ]

    const expected = new UnorderedListToken([
      new UnorderedListItemToken('line1'),
      new UnorderedListToken([
        new UnorderedListItemToken('line2'),
        new UnorderedListToken([
          new UnorderedListItemToken('line3'),
        ]),
      ]),
      new UnorderedListItemToken('line4'),
    ])
    const actual = getUnorderedListFromLineStats(input)

    expect(actual).to.eql(expected)
  })

  it('Only indents for leading space counts that are a factor of 2', () => {
    const input = [
      new LineStats(0, LineType.UnorderedList, 'line1', '-'),
      new LineStats(1, LineType.UnorderedList, 'line2', '-'),
      new LineStats(2, LineType.UnorderedList, 'line3', '-'),
    ]

    const expected = new UnorderedListToken([
      new UnorderedListItemToken('line1'),
      new UnorderedListItemToken('line2'),
      new UnorderedListToken([
        new UnorderedListItemToken('line3')
      ]),
    ])
    const actual = getUnorderedListFromLineStats(input)

    expect(actual).to.eql(expected)
  })

})