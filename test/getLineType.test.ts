import getLineType from '../src/getLineType'
import LineType from '../src/LineType'
import { expect } from 'chai'
import 'mocha'

describe('getLineType tests', () => {

  describe('Headers', () => {

    it('Returns header type', () => {
      const leadingChars = '###'
      const remainder = ''
  
      const result = getLineType(leadingChars, remainder)
  
      expect(result).to.equal(LineType.Header)
    })
  
    it('Ignores header lines with more than 6 hashes', () => {
      const leadingChars = '#######'
      const remainder = ''
  
      const result = getLineType(leadingChars, remainder)
  
      expect(result).to.equal(LineType.Normal)
    })
  
  })

  describe('Ordered lists', () => {

    it('Recognizes single digit lines', () => {
      const leadingChars = '3.'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.OrderedList)
    })

    it('Recognizes multi-digit lines', () => {
      const leadingChars = '2394.'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.OrderedList)
    })

    it('Ignores lines lacking a period', () => {
      const leadingChars = '983'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.Normal)
    })

  })

  describe('Unordered lists', () => {

    it('Recognizes dashes', () => {
      const leadingChars = '-'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.UnorderedList)
    })

    it('Recognizes asterisks', () => {
      const leadingChars = '*'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.UnorderedList)
    })

    it('Recognizes plus signs', () => {
      const leadingChars = '+'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.UnorderedList)
    })

    it('Ignores lines with more than one of the list character', () => {
      const leadingChars = '**'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.Normal)
    })

  })

  describe('Code blocks', () => {

    it('Recognizes three or more backticks', () => {
      const leadingChars = '````'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.CodeBlock)
    })

    it('Ignores less than 3 backticks', () => {
      const leadingChars = '``'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.Normal)
    })

  })

  describe('Quotes', () => {
    
    it('Recognizes a quoted line', () => {
      const leadingChars = '>'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.Quote)
    })

    it('Ignores leading greater than characters with other leading characters', () => {
      const leadingChars = '>>'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.Normal)
    })

  })

  describe('Horizontal rules', () => {

    it('Recognizes lines with more than 2 dashes', () => {
      const leadingChars = '---'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.HorizontalRule)
    })

    it('Ignores lines with 2 dashes or less', () => {
      const leadingChars = '--'
      const remainder = ''

      const result = getLineType(leadingChars, remainder)

      expect(result).to.equal(LineType.Normal)
    })

  })

  it('Recognizes image syntax', () => {
    const leadingChars = ''
    const remainder = '![blah](blah)'

    const result = getLineType(leadingChars, remainder)

    expect(result).to.equal(LineType.Image)
  })

})