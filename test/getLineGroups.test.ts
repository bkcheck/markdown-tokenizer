import getLineGroups from '../src/getLineGroups';
import LineStats from '../src/LineStats';
import { expect } from 'chai';
import 'mocha';
import LineType from '../src/LineType';
import TextToken from '../src/tokens/TextToken';
import QuoteToken from '../src/tokens/QuoteToken';
import UnorderedListToken from '../src/tokens/UnorderedListToken';
import UnorderedListItemToken from '../src/tokens/UnorderedListItemToken';
import OrderedListToken from '../src/tokens/OrderedListToken';
import OrderedListItemToken from '../src/tokens/OrderedListItemToken';
import CodeBlockToken from '../src/tokens/CodeBlockToken';
import HorizontalRuleToken from '../src/tokens/HorizontalRuleToken';
import HeaderToken from '../src/tokens/HeaderToken';
import ImageToken from '../src/tokens/ImageToken';

describe('getLineGroups tests', () => {

  it('Returns an empty group array for an empty stats array', () => {
    const input: LineStats[] = []
    const result = getLineGroups(input)

    expect(result).to.eql([])
  })

  it('Ignores adjacent Empty lines', () => {
    const input: LineStats[] = [
      new LineStats(),
      new LineStats()
    ]

    const result = getLineGroups(input)

    expect(result).to.eql([])
  })

  it('Coalesces adjacent Normal-type lines', () => {
    const input: LineStats[] = [
      new LineStats(0, LineType.Normal, 'line1'),
      new LineStats(0, LineType.Normal, 'line2')
    ]

    const [result] = getLineGroups(input)

    expect(result).to.eql(new TextToken('line1 line2'))
  })

  it('Coalesces adjacent Quote-type lines', () => {
    const input: LineStats[] = [
      new LineStats(0, LineType.Quote, 'line1'),
      new LineStats(0, LineType.Quote, 'line2')
    ]

    const [result] = getLineGroups(input)

    expect(result).to.eql(new QuoteToken('line1 line2'))
  })

  it('Coalesces adjacent UnorderedList-type lines', () => {
    const input: LineStats[] = [
      new LineStats(0, LineType.UnorderedList, 'line1', '-'),
      new LineStats(0, LineType.UnorderedList, 'line2', '-'),
    ]

    const [actual] = getLineGroups(input)
    const expected = new UnorderedListToken([
      new UnorderedListItemToken('line1'),
      new UnorderedListItemToken('line2'),
    ])

    expect(actual).to.eql(expected)
  })

  it('Coalesces adjacent OrderedList-type lines', () => {
    const input: LineStats[] = [
      new LineStats(0, LineType.OrderedList, 'line1', '1.'),
      new LineStats(0, LineType.OrderedList, 'line2', '2.'),
    ]

    const [actual] = getLineGroups(input)
    const expected = new OrderedListToken([
      new OrderedListItemToken('line1'),
      new OrderedListItemToken('line2'),
    ])

    expect(actual).to.eql(expected)
  })

  it('Correctly processes code blocks', () => {
    const input: LineStats[] = [
      new LineStats(0, LineType.CodeBlock, '', '```javascript'),
      new LineStats(0, LineType.Normal, 'text'),
    ]

    const [actual] = getLineGroups(input)
    const expected = new CodeBlockToken('javascript')
    expected.add(new TextToken('text'))

    expect(actual).to.eql(expected)
  })

  it('Correctly processes horizontal rules', () => {
    const input: LineStats[] = [
      new LineStats(0, LineType.Normal, 'text'),
      new LineStats(0, LineType.HorizontalRule),
    ]

    const actual = getLineGroups(input)
    const expected = [
      new TextToken('text'),
      new HorizontalRuleToken()
    ]

    expect(actual).to.eql(expected)
  })

  it('Correctly processes header lines', () => {
    const input: LineStats[] = [
      new LineStats(0, LineType.Header, 'line1', '###'),
      new LineStats(0, LineType.Header, 'line2', '#'),
    ]

    const actual = getLineGroups(input)
    const expected = [
      new HeaderToken(3, 'line1'),
      new HeaderToken(1, 'line2'),
    ]

    expect(actual).to.eql(expected)
  })

  describe('Image tests', () => {

    it('Correctly processes image lines', () => {
      const input: LineStats[] = [
        new LineStats(0, LineType.Image, '![text](url title)'),
      ]
  
      const actual = getLineGroups(input)
      const expected = [new ImageToken('url', 'text', 'title')]
    })

    it('Correctly processes image lines', () => {
      const input: LineStats[] = [
        new LineStats(0, LineType.Image, '![text](url title)'),
      ]
  
      const actual = getLineGroups(input)
      const expected = [new ImageToken('url', 'text', 'title')]
    })

  })

})