import getLineArray from '../src/getLineArray';
import { expect } from 'chai';
import 'mocha';

describe('getLineArray tests', () => {

  it('Should split input string on return characters', () => {
    const input = 'Line1\rLine2'
    const result = getLineArray(input)

    expect(result).to.eql(['Line1', 'Line2'])
  })

  it('Should split input string on line feed characters', () => {
    const input = 'Line1\nLine2'
    const result = getLineArray(input)

    expect(result).to.eql(['Line1', 'Line2'])
  })

  it('Should preserve empty lines', () => {
    const input = 'Line1\n\nLine2'
    const result = getLineArray(input)

    expect(result).to.eql(['Line1', null, 'Line2'])
  })

})
