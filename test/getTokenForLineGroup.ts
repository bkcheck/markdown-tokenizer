import BaseToken from '../src/tokens/BaseToken'
import CodeBlockToken from '../src/tokens/CodeBlockToken'
import HeaderToken from '../src/tokens/HeaderToken'
import HorizontalRuleToken from '../src/tokens/HorizontalRuleToken'
import ImageToken from '../src/tokens/ImageToken'
import LineGroup from '../src/LineGroup'
import LineStats from '../src/LineStats'
import LineType from '../src/LineType'
import OrderedListItemToken from '../src/tokens/OrderedListItemToken'
import OrderedListToken from '../src/tokens/OrderedListToken'
import QuoteToken from '../src/tokens/QuoteToken'
import TextToken from '../src/tokens/TextToken'
import UnorderedListItemToken from '../src/tokens/UnorderedListItemToken'
import UnorderedListToken from '../src/tokens/UnorderedListToken'

import getTokenForLineGroup from '../src/getTokenForLineGroup'
import { expect } from 'chai';
import 'mocha';

describe('processLineGroup tests', () => {

  it('Correctly processes a Header token', () => {
    const input = new LineGroup(LineType.HorizontalRule, new LineStats(0, LineType.Header, 'text', '###'))

    const expected = new HeaderToken(3, 'text')
    const actual = getTokenForLineGroup(input)

    expect(actual).to.eql(expected)
  })

  it('Correctly processes a Horizontal Rule token', () => {
    const input = new LineGroup(LineType.HorizontalRule, new LineStats(0, LineType.HorizontalRule))

    const expected = new HorizontalRuleToken()
    const actual = getTokenForLineGroup(input)

    expect(actual).to.eql(expected)
  })

  it('Correctly processes an Image token', () => {
    const input = new LineGroup(LineType.Image, new LineStats(0, LineType.Image, '![text](url title)'))

    const expected = new ImageToken('url', 'text', 'title')
    const actual = getTokenForLineGroup(input)

    expect(actual).to.eql(expected)
  })

  it('Correctly processes a Normal token', () => {
    const input = new LineGroup(LineType.Image)
    input.add(new LineStats(0, LineType.Normal, 'text1'))
    input.add(new LineStats(0, LineType.Normal, 'text2'))

    const expected = new TextToken('text1 text2')
    const actual = getTokenForLineGroup(input)

    expect(actual).to.eql(expected)
  })

  it('Correctly processes a Quote token', () => {
    const input = new LineGroup(LineType.Quote)
    input.add(new LineStats(0, LineType.Quote, 'text1'))
    input.add(new LineStats(0, LineType.Quote, 'text2'))

    const expected = new QuoteToken('text1 text2')
    const actual = getTokenForLineGroup(input)

    expect(actual).to.eql(expected)
  })

  it('Correctly processes a CodeBlock token', () => {
    const input = new LineGroup(LineType.CodeBlock)
    input.add(new LineStats(0, LineType.CodeBlock))
    input.add(new LineStats(0, LineType.Normal, 'text1'))
    input.add(new LineStats(0, LineType.Normal, '  text2'))
    input.add(new LineStats(0, LineType.CodeBlock))

    const expected = new CodeBlockToken(null, [
      new TextToken('text1'),
      new TextToken('  text2'),
    ])
    const actual = getTokenForLineGroup(input)

    expect(actual).to.eql(expected)
  })

})