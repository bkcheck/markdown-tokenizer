import IntermediateTextToken, { IntermediateTextTokenType } from '../src/tokens/IntermediateTextToken'
import getTextTokenStack from '../src/getTextTokenStack'
import { expect } from 'chai'
import 'mocha'

describe('getTextTokenStack tests', () => {

  it('Parses normal text', () => {
    const input = 'text'

    const expected = [
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: 'text' })
    ]
    const actual = getTextTokenStack(input)

    expect(actual).to.eql(expected)
  })

  it('Parses emphasized text', () => {
    const input = 'outside ***inside***'
    
    const expected = [
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: 'outside '}),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Emphasis, length: 3, char: '*' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: 'inside' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Emphasis, length: 3, char: '*' }),
    ]
    const actual = getTextTokenStack(input)

    expect(actual).to.eql(expected)
  })

  it('Ignores delimited emphasizing tokens', () => {
    const input = '\\__text'
    
    const expected = [
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: '_' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Emphasis, length: 1, char: '_' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: 'text' }),
    ]
    const actual = getTextTokenStack(input)

    expect(actual).to.eql(expected)
  })

  it('Parses strikethrough text', () => {
    const input = '~~text~~'

    const expected = [
      new IntermediateTextToken({ type: IntermediateTextTokenType.Strikethrough, length: 2, char: '~' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: 'text' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Strikethrough, length: 2, char: '~' }),
    ]
    const actual = getTextTokenStack(input)

    expect(actual).to.eql(expected)
  })

  it('Treats single tildes as normal text', () => {
    const input = '~text~'

    const expected = [
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: '~text~' })
    ]
    const actual = getTextTokenStack(input)

    expect(actual).to.eql(expected)
  })

  it('Parses backticks as inline code', () => {
    const input = '``text``'

    const expected = [
      new IntermediateTextToken({ type: IntermediateTextTokenType.Code, length: 2, char: '`' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Text, text: 'text' }),
      new IntermediateTextToken({ type: IntermediateTextTokenType.Code, length: 2, char: '`' }),
    ]
  })

})