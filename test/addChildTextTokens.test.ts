import EmphasisTextToken from '../src/tokens/EmphasisTextToken'
import InlineCodeToken from '../src/tokens/InlineCodeToken'
import StrikethroughTextToken from '../src/tokens/StrikethroughTextToken'
import TextToken from '../src/tokens/TextToken'
import IntermediateTextToken, { IntermediateTextTokenType } from '../src/tokens/IntermediateTextToken'

import addChildTextTokens from '../src/addChildTextTokens'
import { expect } from 'chai'
import 'mocha'

describe('addChildTextTokens tests', () => {

  it('Adds normal text as innerText on the parent', () => {
    const inputText = 'text'
    const inputToken = new TextToken()

    const expectedChildren: TextToken[] = []
    const expectedInnerText = 'text'

    const { innerText, children } = addChildTextTokens(inputText, inputToken)

    expect(innerText).to.eql(expectedInnerText)
    expect(children).to.eql(expectedChildren)
  })

  it('Adds emphasis tokens as child tokens', () => {
    const inputText = 'text **emphasized**'
    const inputToken = new TextToken()

    const expectedChildren = [
      new TextToken('text '),
      new EmphasisTextToken(2, 'emphasized'),
    ]
    const expectedInnerText = ''

    const { innerText, children } = addChildTextTokens(inputText, inputToken)

    expect(innerText).to.eql(expectedInnerText)
    expect(children).to.eql(expectedChildren)
  })

  it('Caps emphasis level at 3', () => {
    const inputText = '****emphasized****'
    const inputToken = new TextToken()

    const expectedChildren = [
      new EmphasisTextToken(3, 'emphasized'),
    ]
    const expectedInnerText = ''

    const { innerText, children } = addChildTextTokens(inputText, inputToken)

    expect(innerText).to.eql(expectedInnerText)
    expect(children).to.eql(expectedChildren)
  })

  it('Correctly parses nested emphasis tokens', () => {
    const inputText = '*text1 **text2** text3*'
    const inputToken = new TextToken()

    const expectedChildren = [
      new EmphasisTextToken(1, null, [
        new TextToken('text1 '),
        new EmphasisTextToken(2, 'text2'),
        new TextToken(' text3'),
      ])
    ]
    const expectedInnerText = ''

    const { innerText, children } = addChildTextTokens(inputText, inputToken)

    expect(innerText).to.eql(expectedInnerText)
    expect(children).to.eql(expectedChildren)
  })

  it('Correctly parses nested tokens of different types', () => {
    const inputText = '`code ***bold ~~all~~*** ~~_boldInStrikethrough_~~` normal'
    const inputToken = new TextToken()

    const expectedChildren = [
      new InlineCodeToken(null, [
        new TextToken('code '),
        new EmphasisTextToken(3, null, [
          new TextToken('bold '),
          new StrikethroughTextToken('all'),
        ]),
        new TextToken(' '),
        new StrikethroughTextToken(null, [
          new EmphasisTextToken(1, 'boldInStrikethrough'),
        ]),
      ]),
      new TextToken(' normal')
    ]
    const expectedInnerText = ''

    const { innerText, children } = addChildTextTokens(inputText, inputToken)

    expect(innerText).to.eql(expectedInnerText)
    expect(children).to.eql(expectedChildren)
  })

})
